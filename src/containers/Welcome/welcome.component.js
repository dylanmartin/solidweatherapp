import React from 'react';
import { Uploader } from '@inrupt/solid-react-components';
import isLoading from '@hocs/isLoading';
import { Trans, withTranslation } from 'react-i18next';
import {
  WelcomeWrapper,
  WelcomeCard,
  HourlyWeatherCard,
  WeeklyWeatherCard,
  WeatherContainer,
  LocationInputContainer,
  GoButton
} from './welcome.style';
import { Container } from 'react-bootstrap';
import { withToastManager } from 'react-toast-notifications';
import { ImageProfile } from '@components';
import { Calendar } from 'react-calendar';
import $rdf from "rdflib";
import { get } from 'https';
import { keyframes } from 'emotion';
import './results.css';
import { min, updateLocale } from 'moment';
import { deleteTriples } from '../../parse';
import LoadingScreen from '../loadingSrceen';
import { directive } from '@babel/types';
import WeeklyWeather from './WeeklyWeather/WeeklyWeatherComponent';
import HourlyWeather from './HourlyWeather/HourlyWeatherComponent';
import DisplayNav from './NavTabs/DisplalyNav';
import EventDisplay from './EventDisplay/EventDisplay';
var CUPurl;
var updateLocation;
var loadSetter;
var eventSetter;
// RG - 2019-02-28
// Loads the data from a URL into the local store
const loadFromUrl = (url, store) => {
  return new Promise((resolve, reject) => {
    let fetcher = new $rdf.Fetcher(store);
    try {
      fetcher.load(url).then(response => {
        resolve(response.responseText);
        // console.debug(response.responseText);
        // $rdf.parse(response.responseText, store, $rdf.sym(url).uri,"application/rdf");
      });
    } catch (err) {
      reject(err);
    }
  });
};

// RG - 2019-02-28
// Prepares a query by converting SPARQL into a Solid query
const prepare = (qryStr, store) => {
  return new Promise((resolve, reject) => {
    try {
      let query = $rdf.SPARQLToQuery(qryStr, false, store);
      resolve(query);
    } catch (err) {
      reject(err);
    }
  });
};

// RG - 2019-02-28
// Executes a query on the local store
const execute = (qry, store) => {
  return new Promise((resolve, reject) => {
    // console.debug("here");
    const wanted = qry.vars;
    const resultAry = [];
    store.query(
      qry,
      results => {
        // console.debug("here1");
        if (typeof results === "undefined") {
          reject("No results.");
        } else {
          let row = rowHandler(wanted, results);
          // console.debug(row);
          if (row) resultAry.push(row);
        }
      },
      {},
      () => {
        resolve(resultAry);
      }
    );
  });
};

// RG - 2019-02-28
// Puts query results into an array according to the projection
const rowHandler = (wanted, results) => {
  const row = {};
  for (var r in results) {
    let found = false;
    let got = r.replace(/^\?/, "");
    if (wanted.length) {
      for (var w in wanted) {
        if (got === wanted[w].label) {
          found = true;
          continue;
        }
      }
      if (!found) continue;
    }
    row[got] = results[r].value;
  }
  return row;
};

async function getData(date) {
  // loading new events
  let query = '';
  let store = $rdf.graph();
  loadFromUrl(CUPurl, store).then(() =>
    prepare(query, store).then(qry =>
      execute(qry, store).then(results => {
        // do things with results 
      })
    )
  );
}


/**
 * Welcome Page UI component, containing the styled components for the Welcome Page
 * Image component will get theimage context and resolve the value to render.
 * @param props
 */
const iterateProps = row => {
  var str = "";
  for (const k in row) {
    if (row.hasOwnProperty(k)) {
      if (str.length > 0) str += "; ";
      str += row[k];
    }
  }
  return str;
};

const getResults = results => {
  if (results) {
    return results.map(r => <p>{iterateProps(r)}</p>);
  } else {
    return "No results";
  }
};

async function getDate(value) {
  getData(value);
  // document.getElementById("datediv").innerHTML = "the selected date is: " + value.toString();
}

function renderWeeklyWeather(data) {
  let elements = [];
  let now = new Date();
  let day = now.getDay();
  for (let i = 1; i < data.length; i++) {
    elements.push(<WeeklyWeather key={'weekly' + i} index={i} day={day + i} condition={data[i].description} temperature={data[i].temp} otherInfo={data[i].otherInfo} />)
  }
  return (elements);
}

async function updateLoc() {
  console.log('running update');
  document.getElementById('inputErrorMessage').innerHTML = '';
  let input = document.getElementById('locationInput').value;
  console.log(input);
  if (input === '') {
    document.getElementById('inputErrorMessage').innerHTML = 'please enter a city and a state';
  } else {
    let arr = input.split(" ");
    let state = arr[arr.length-1];
    let city = '';
    for (let x = 0; x < arr.length-1;x++){
      city += arr[x] + ' ';
    }
    eventSetter(false);
    await loadSetter(true);
    await updateLocation(city.trim(),state);
    await loadSetter(false);
  }
}

const WelcomePageContent = props => {
  var { name, t } = props;
  var events = props.events;
  var sd = props.selectedDate;
  loadSetter = props.loadSetter;
  updateLocation = props.updateLocation;
  eventSetter = props.eventSetter;
  console.log(props.city.toString())
  if (props.webId !== undefined) {
    // CUPurl = props.webId.replace('profile/card#me', '') + 'private/events#';
    CUPurl = props.webId;
  }
  return (
    // props.isLoading ? <WelcomeWrapper data-testid="welcome-wrapper"><LoadingScreen /> </WelcomeWrapper> :
    <WelcomeWrapper data-testid="welcome-wrapper">
      <WelcomeCard className="card">
        <h3>
          {t('welcome.welcome')}, <span>{name}</span>
        </h3>
        <p>
          Now showing you the weather in <span>{props.city}, {props.state}</span>
        </p>
        <LocationInputContainer>
          <p>Show me weather in <input id='locationInput' placeholder="city state" /><GoButton onClick={() => updateLoc()}>Go!</GoButton></p>
          <span id='inputErrorMessage' />
        </LocationInputContainer>
      </WelcomeCard>
      <WeatherContainer>
        <HourlyWeatherCard className='card'>
          <HourlyWeather data={props.hourlyForecast} />
        </HourlyWeatherCard>
        <WeeklyWeatherCard className='card'>
          <DisplayNav eventSetter={props.eventSetter}/>
          {props.showEvents ? (<EventDisplay city={props.city} state={props.state} events={props.events}/>) : (renderWeeklyWeather(props.weeklyForecast))}
        </WeeklyWeatherCard>
      </WeatherContainer>
    </WelcomeWrapper>
  );
};


export { WelcomePageContent };
export default withTranslation()(
  isLoading(withToastManager(WelcomePageContent))
);
