import React from 'react';
import './EventDisplay.css';
const states = require('us-state-codes');

export default class EventDisplay extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            results: [],
            state: props.state,
            city: props.city,
        }
        this.renderEvents = this.renderEvents.bind(this);
        this.handleOver = this.handleOver.bind(this);
        this.handleOut = this.handleOut.bind(this);
    }

    handleOver(object) {
        let startTime = object.eventStartTime;
        let times = document.getElementsByClassName('hourDataDisplay');
        for (let x = 0; x < times.length; x++) {
            let prefix = "AM";
            let sTime = parseInt(startTime.split('T')[1].split(':')[0]);
            if(sTime > 11){
                prefix = "PM";
            }
            sTime = sTime % 12;
            if (sTime === 0) {
                sTime = 12;
            }
            if (times[x].innerHTML === sTime.toString() + ' ' + prefix) {
                let containers = document.getElementsByClassName('hourlyBaseContainer');
                this.setState({ index: x })
                containers[x].style.borderStyle = 'solid';
                containers[x].scrollIntoView();
            }
        }
    }

    handleOut() {
        let containers = document.getElementsByClassName('hourlyBaseContainer');
        containers[this.state.index].style.borderStyle = 'hidden';
    }

    async componentDidMount() {
        let results = this.props.events;
        let filteredResults = []
        for (let i = 0; i < results.length; i++) {

            // translate statecode to full state name
            if (this.state.state.length > 2) {
                await this.setState({ state: states.getStateCodeByStateName(this.state.state) });
            }
            if (results[i].eventCity.toLowerCase() === this.state.city.toLowerCase() && results[i].eventState.toLowerCase() === this.state.state.toLowerCase()) {
                filteredResults.push(results[i])
            }
        }
        this.setState({ results: filteredResults })
        console.log(filteredResults)
    }

    renderEvents() {
        let todaysEvents = this.state.results.map((obj) => (
            <div className='container' onMouseOut={() => this.handleOut()} onMouseOver={() => this.handleOver(obj)}>
                <div className='baseInformation'>
                    <div className='event'>
                        Name: {obj.eventName}
                    </div>
                    <div className='event'>
                        Location: {obj.eventLocation}
                    </div>
                    <div className='event'>
                        Address: {obj.eventStreetAddress}
                    </div>
                    <div className='event'>
                        City: {obj.eventCity}
                    </div>
                    <div className='event'>
                        State: {obj.eventState}
                    </div><div className='event'>
                        Zipcode: {obj.eventZipCode}
                    </div><div className='event'>
                        Start Time: {obj.eventStartTime}
                    </div><div className='event'>
                        End Time: {obj.eventEndTime}
                    </div>
                    <div className='event'>
                        <pre className='descriptionDisplay'>
                            {obj.eventDescription}
                        </pre>
                    </div>
                </div>
            </div>
        ));
        if(todaysEvents.length === 0){
            return "You have no events here planned today"
        }
        return todaysEvents;
    }

    render() {
        return (
            <div className='eventContainer'>
                <div className='dateDisplay'>
                    <h3 className='dateTextDisplay'>
                        Events for {new Date().toDateString()}
                    </h3>
                    <h3 className='dateTextDisplay'>
                        {this.state.city} {this.state.state}
                    </h3>
                </div>
                <div>
                    {this.renderEvents()}
                </div>
            </div>
        );
    }
}