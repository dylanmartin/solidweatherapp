import styled from 'styled-components';
import { media } from '../../utils';


export const WelcomeWrapper = styled.section`
  top:0;
  bottom:0;
  overflow-y:auto;
  overflow-x:visible;
  position:relative;
  padding: 0;
  width:100%;
  height:100%;
  align-items: center;
  justify-content: center;
  h3 {
    color: #666666;
    span {
      font-weight: bold;
    }
    a {
      font-size: 1.9rem;
    }
  }
`;

export const WelcomeCard = styled.div`
  background-color: #fff;
  
  margin: 30px auto;
  max-width:1200px;

  //Overriding the style guide card flexbox settings
  flex-direction: column !important;
  padding: 0 !important; //temporary fix to a style guide bug

  align-items: center;

  a {
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }

  button {
    margin-left: 8px;
  }
`;

export const WeatherContainer = styled.div`
  max-width:1200px;
  display: flex;
  position:relative;
  flex-direction: row !important;
  align-items: center;
  margin: 30px auto;
  height:60% !important;
`;

export const LocationInputContainer = styled.div`
  flex-direction: row;
`

export const LocationInput = styled.div`

`

export const GoButton = styled.button`
  background-color: #333333;
  color: #eeeeee;
`

export const WeeklyWeatherCard = styled.div` 
  // background-color: #fff;
  padding:0px !important;
  overflow-y:auto;
  //Overriding the style guide card flexbox settings
  flex-direction: row !important;
  border:0px !important;
  width:50%;
  position:relative;
  height:600px;
  margin-left: 2.5%;
`;

export const HourlyWeatherCard = styled.div` 
  background-color: #fff;
  position:relative;
  padding:0px !important;
  overflow-y:auto;
  //Overriding the style guide card flexbox settings
  border:0px !important;
  width:50%;
  margin-right: 2.5%;
  height:600px;
`;

export const WelcomeLogo = styled.div`
  width: 50%;
  height: 100%;
  img {
    width: 60%;
    display: block;
    margin: 0 auto;
  }
`;

export const WelcomeProfile = styled.div`
  height: 100%;
  text-align: center;
  position: relative;

  img {
    width: 120px;
    height: 120px;
    border-radius: 50%;
  }

  h1,
  img {
    margin: 0 10px;
    display: inline-block;
    vertical-align: middle;
  }

  ${media.tablet`
    width: 50%;
    &:after {
      display: block;
      content: "";
      position: absolute;
      height: 100%;
      width: 1px;
      background-color:#D0D0D0;
      top:0;
    }
  `}
`;

export const ImageWrapper = styled.div`
display: flex;
justify-content: center;
align-items: center;

button {
  margin-left: 0px;
}
`;

export const ImageContainer =  styled.div`
  background-image: ${({image}) => image ? `url(${image})`: '#cccccc'};
  background-size: cover;
  border-radius: 50%;
  width: 128px;
  height: 128px;
  `;

export const WelcomeDetail = styled.div`
  padding: 1rem 3.5rem;

  p,
  li {
    color: #666666;
  }
  ul {
    list-style: disc;
    margin: 0 18px;
  }
`;
