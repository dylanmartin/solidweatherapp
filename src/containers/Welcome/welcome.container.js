import React, { Component } from 'react';
import WelcomePageContent from './welcome.component';
import { withWebId } from '@inrupt/solid-react-components';
import data from '@solid/query-ldflex';
import { withToastManager } from 'react-toast-notifications';
import auth from "solid-auth-client";
import { promised } from 'q';
import { templateElement } from '@babel/types';

const defaultProfilePhoto = '/img/icon/empty-profile.svg';
var CUPurl;
var EventUrl
const $rdf = require("rdflib");
const store = $rdf.graph();
const fetcher = new $rdf.Fetcher(store);

const eventQuery = `select distinct ?SocialAct ?eventName ?eventDescription ?eventZipCode ?eventCity ?eventStreetAddress ?eventLocation ?eventState ?eventEndTime ?eventStartTime
where{
  ?SocialAct a <http://www.ontologyrepository.com/CommonCoreOntologies/SocialAct>.
  ?SocialAct <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?DesignativeName1.
  ?SocialAct <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/occurs_at> ?Facility.
  ?SocialAct <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/described_by> ?DiscriptiveInformationContentEntity.
  ?SocialAct <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/occurs_on> ?Time.
  ?DesignativeName1 a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?DesignativeName1 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro1.
  ?Ro1 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro1 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/has_text_value> ?eventName.
  ?DiscriptiveInformationContentEntity a <http://www.ontologyrepository.com/CommonCoreOntologies/DescriptiveInformationContentEntity>.
  ?DiscriptiveInformationContentEntity <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro2.
  ?Ro2 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro2 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventDescription.
  ?Facility a <http://www.ontologyrepository.com/CommonCoreOntologies/Facility>.
  ?Facility <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?StreetAddress, ?DesignativeName3.
  ?Facility <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0001025> ?GeospatialRegion.
  ?GeospatialRegion a <http://www.ontologyrepository.com/CommonCoreOntologies/GeospatialRegion>.
  ?GeospatialRegion <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/BFO_0000050> ?PostalZone.
  ?GeospatialRegion <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/BFO_0000050> ?LocalAdministrativeRegion.
  ?PostalZone a  <http://www.ontologyrepository.com/CommonCoreOntologies/PostalZone>.
  ?PostalZone <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?ZipCode.
  ?ZipCode <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro3.
  ?Ro3 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro3 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventZipCode.
  ?LocalAdministrativeRegion a <http://www.ontologyrepository.com/CommonCoreOntologies/LocalAdministrativeRegion>.
  ?LocalAdministrativeRegion <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?DesignativeName2.
  ?LocalAdministrativeRegion <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/BFO_0000050> ?State.
  ?DesignativeName2 a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?DesignativeName2 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro4.
  ?Ro4 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventCity.
  ?StreetAddress a <http://www.ontologyrepository.com/CommonCoreOntologies/StreetAddress>.
  ?StreetAddress <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro5.
  ?Ro5 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro5 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventStreetAddress.
  ?DesignativeName3 a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?DesignativeName3 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro6.
  ?Ro6 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro6 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventLocation.
  ?State a <http://www.ontologyrepository.com/CommonCoreOntologies/State>.
  ?State <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?AbbreviatedName.
  ?AbbreviatedName a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?AbbreviatedName <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro7.
  ?Ro7 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro7 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventState.
  ?Time a <http://purl.obolibrary.org/obo/BFO_0000038>.
  ?Time <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/interval_finished_by> ?EndTime.
  ?Time <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/interval_started_by> ?StartTime.
  ?EndTime a <http://purl.obolibrary.org/obo/BFO_0000038>.
  ?EndTime <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/designated_by> ?CodeIdentifier1.
  ?CodeIdentifier1 a <http://www.ontologyrepository.com/CommonCoreOntologies/CodeIdentifier>.
  ?CodeIdentifier1 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro8.
  ?Ro8 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro8 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/has_datetime_value> ?eventEndTime.
  ?StartTime a <http://purl.obolibrary.org/obo/BFO_0000038>.
  ?StartTime <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/designated_by> ?CodeIdentifier2.
  ?CodeIdentifier2 a <http://www.ontologyrepository.com/CommonCoreOntologies/CodeIdentifier>.
  ?CodeIdentifier2 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro9.
  ?Ro9 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro9 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/has_datetime_value> ?eventStartTime.
}`;

const addressQuery = `SELECT ?city ?state ?streetAddress
WHERE {
  ?Person a <http://www.ontologyrepository.com/CommonCoreOntologies/Person>.
  ?Person <http://www.ontologyrepository.com/CommonCoreOntologies/agent_in> ?ActOfOwnership.
  ?ActOfOwnership a <http://www.ontologyrepository.com/CommonCoreOntologies/ActOfOwnership>.
  ?ActOfOwnership <http://www.ontologyrepository.com/CommonCoreOntologies/has_object> ?ResidentialFacility.
  ?ResidentialFacility a <http://www.ontologyrepository.com/CommonCoreOntologies/ResidentialFacility>.
  ?ResidentialFacility <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?StreetAddress.
  ?ResidentialFacility <http://purl.obolibrary.org/obo/RO_0001025> ?GeospatReg.
  ?StreetAddress a <http://www.ontologyrepository.com/CommonCoreOntologies/StreetAddress>.
  ?StreetAddress <http://purl.obolibrary.org/obo/RO_0010001> ?IBE1.
  ?IBE1 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?IBE1 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?streetAddress.
  ?GeospatReg a <http://www.ontologyrepository.com/CommonCoreOntologies/GeospatialRegion>.
  ?GeospatReg <http://purl.obolibrary.org/obo/BFO_0000050> ?LocalAdminRegion.
  ?LocalAdminRegion a <http://www.ontologyrepository.com/CommonCoreOntologies/LocalAdministrativeRegion>.
  ?LocalAdminRegion <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?DesName.
  ?LocalAdminRegion <http://purl.obolibrary.org/obo/BFO_0000050> ?State.
  ?DesName a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?DesName <http://purl.obolibrary.org/obo/RO_0010001> ?IBE2.
  ?IBE2 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?city.
  ?State a <http://www.ontologyrepository.com/CommonCoreOntologies/State>.
  ?State <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?DesName2.
  ?DesName2 a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?DesName2 <http://purl.obolibrary.org/obo/RO_0010001> ?IBE3.
  ?IBE3 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?state.
}`

// const latLngQuery = `SELECT ?lat ?lng
// WHERE {
//   ?Person a <http://www.ontologyrepository.com/CommonCoreOntologies/Person>.
//   ?Person <http://www.ontologyrepository.com/CommonCoreOntologies/agent_in> ?AOR.
//   ?AOR a <http://www.ontologyrepository.com/CommonCoreOntologies/ActOfResiding>.
//   ?AOR <http://www.ontologyrepository.com/CommonCoreOntologies/has_object> ?ResFacility.
//   ?ResFacility a <http://www.ontologyrepository.com/CommonCoreOntologies/ResidentialFacility>.
//   ?ResFacility <http://purl.obolibrary.org/obo/RO_0001025> ?GeospatialRegion.
//   ?GeospatialRegion a <http://www.ontologyrepository.com/CommonCoreOntologies/GeospatialRegion>.
//   ?GeospatialRegion <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?GeospatialRegionIdentifier.
//   ?GeospatialRegionIdentifier a <http://www.ontologyrepository.com/CommonCoreOntologies/GeospatialRegionIdentifier>.
//   ?GeospatialRegionIdentifier <http://purl.obolibrary.org/obo/RO_0010001> ?IBE2.
//   ?IBE2 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
//   ?IBE2 <http://www.ontologyrepository.com/CommonCoreOntologies/has_longitude_value> ?lng.
//   ?IBE2 <http://www.ontologyrepository.com/CommonCoreOntologies/has_latitude_value> ?lat.
// }
// `
const apiKey = '0pm7SOWgNPPTG14tgjIZG1zG0lc3Aqu0';
/**
 * Container component for the Welcome Page, containing example of how to fetch data from a POD
 */
class WelcomeComponent extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      image: defaultProfilePhoto,
      isLoading: true,
      hasImage: false,
      selectedDate: new Date(),
      weeklyForecast: [],
      hourlyForecast: [],
      state: '',
      city: '',
      events: [],
      showEvents: false,
    };
    this.isLoadingSetter = this.isLoadingSetter.bind(this);
    this.setDate = this.setDate.bind(this);
    this.addressToCords = this.addressToCords.bind(this);
    this.getHourlyForcast = this.getHourlyForcast.bind(this);
    this.getDailyForcast = this.getDailyForcast.bind(this);
    this.getData = this.getData.bind(this);
    this.loadFromUrl = this.loadFromUrl.bind(this);
    this.prepare = this.prepare.bind(this);
    this.rowHandler = this.rowHandler.bind(this);
    this.execute = this.execute.bind(this);
    this.updateLocation = this.updateLocation.bind(this);
    this.getEventData = this.getEventData.bind(this);
    this.eventSetter = this.eventSetter.bind(this);
  }

  async updateLocation(city, state) {
    return new Promise(
      async (resolve, reject) => {
        await this.setState({
          city: city,
          state: state,
        });
        // convert the users address to lat and long and assign the values to this.state.lat and this.state.lng
        await this.addressToCords();
        // get the daily forcast for specified location
        await this.getDailyForcast();
        // get hourly forecast for current day at specified location
        await this.getHourlyForcast();
        resolve();
      }
    )
  }

  // RG - 2019-02-28
  // Loads the data from a URL into the local store
  loadFromUrl(url, store) {
    return new Promise((resolve, reject) => {
      let fetcher = new $rdf.Fetcher(store);
      try {
        fetcher.load(url).then(response => {
          resolve(response.responseText);
          // console.debug(response.responseText);
          // $rdf.parse(response.responseText, store, $rdf.sym(url).uri,"application/rdf");
        });
      } catch (err) {
        reject(err);
      }
    });
  };

  // RG - 2019-02-28
  // Prepares a query by converting SPARQL into a Solid query
  prepare(qryStr, store) {
    return new Promise((resolve, reject) => {
      try {
        let query = $rdf.SPARQLToQuery(qryStr, false, store);
        resolve(query);
      } catch (err) {
        reject(err);
      }
    });
  };

  // RG - 2019-02-28
  // Executes a query on the local store
  execute(qry, store) {
    return new Promise((resolve, reject) => {
      // console.debug("here");
      const wanted = qry.vars;
      const resultAry = [];
      store.query(
        qry,
        results => {
          // console.debug("here1");
          if (typeof results === "undefined") {
            reject("No results.");
          } else {
            let row = this.rowHandler(wanted, results);
            // console.debug(row);
            if (row) resultAry.push(row);
          }
        },
        {},
        () => {
          resolve(resultAry);
        }
      );
    });
  };

  // RG - 2019-02-28
  // Puts query results into an array according to the projection
  rowHandler(wanted, results) {
    const row = {};
    for (var r in results) {
      let found = false;
      let got = r.replace(/^\?/, "");
      if (wanted.length) {
        for (var w in wanted) {
          if (got === wanted[w].label) {
            found = true;
            continue;
          }
        }
        if (!found) continue;
      }
      row[got] = results[r].value;
    }
    return row;
  };

  async getData() {
    return new Promise(
      (resolve, reject) => {
        let store = $rdf.graph();
        this.loadFromUrl(CUPurl, store).then(() =>
          this.prepare(addressQuery, store).then(qry =>
            this.execute(qry, store).then(async results => {
              var user = data[CUPurl];
              if (results.length !== 0) {
                await this.setState(
                  {
                    city: results[0].city,
                    state: results[0].state,
                    streetAddress: results[0].streetAddress,
                  }
                );
              } 
              else{
                console.log('No CUP found, using default card')
                let city = await user.vcard_hasAddress.vcard_locality;
                let stateval = await user.vcard_hasAddress.vcard_region;
                let address = await user.vcard_hasAddress["vcard:street-address"];
                await this.setState({ 
                  streetAddress: address.toString(),
                  city: city.toString(),
                  state: stateval.toString(),
                 });
              }
              resolve();
            })
          )
        );
      }
    )
  }

  async getEventData(date) {
    return new Promise(
      async (resolve,reject) => {
        // loading new events
        let store = $rdf.graph();
        this.loadFromUrl(EventUrl, store).then(() =>
          this.prepare(eventQuery, store).then(qry =>
            this.execute(qry, store).then(async results => {
              await this.setState({events:results});
              resolve();
            })
          )
        );
      }
    )
    
  }

  async addressToCords() {
    return new Promise(
      async (resolve, reject) => {
        let res = await fetch(`http://www.mapquestapi.com/geocoding/v1/address?key=${apiKey}&location=${this.state.streetAddress.replace(/ /g, '+')},${this.state.city},${this.state.state}`);
        let json = await res.json();
        let lat = json.results[0].locations[0].latLng.lat;
        let lng = json.results[0].locations[0].latLng.lng;
        this.setState({ lat: lat, lng: lng });
        resolve();
      }
    )

  }
  async getHourlyForcast() {
    let hourlyForecast = []
    return new Promise(
      async (resolve, reject) => {
        let res = await fetch(`https://api.weather.gov/points/${this.state.lat},${this.state.lng}/forecast/hourly`, { method: "GET" });
        let response = await res.json();

        var weather = response.properties.periods;
        // get 12 hour forecast
        for (let i = 0; i < 12; i++) {
          let hour = weather[i].startTime.split("T")[1].split(':')[0];
          let condition = weather[i].shortForecast;
          let temp = `${weather[i].temperature} ˚${weather[i].temperatureUnit}`
          hourlyForecast.push(
            {
              hour: hour,
              condition: condition,
              temp: temp,
            }
          );
        }
        this.setState({ hourlyForecast: hourlyForecast });
        resolve();
      }
    )
  }

  async getDailyForcast() {
    let weeklyForecast = []
    return new Promise(
      async (resolve, reject) => {
        let response = await fetch(`https://api.weather.gov/points/${this.state.lat},${this.state.lng}/forecast`, { method: "GET" });
        let res = await response.json();
        var weather = res.properties.periods;

        // Currently iterating over BOTH day and night
        // Get forecast for every day of thee week except the current day 
        for (var i = 0; i < weather.length; i += 2) {

          let temp = `${weather[i].temperature} ˚${weather[i].temperatureUnit}`;
          //Short description of the weather for that 12 hour period, longest one I saw was about 40 characters
          let description = weather[i].shortForecast;
          let descriptiveDescription = weather[i].detailedForecast;
          let windSpeed = `${weather[i].windSpeed} ${weather[i].windDirection}`
          weeklyForecast.push(
            {
              temp: temp,
              description: description,
              otherInfo: {
                descriptiveDescription: descriptiveDescription,
                windSpeed: windSpeed,
              }
            }
          );
        }
        this.setState({ weeklyForecast: weeklyForecast })
        resolve();
      }
    )
  }

  eventSetter(bool){
    this.setState({showEvents:bool})
  }

  async isLoadingSetter(bool) {
    return new Promise(
      async (resolve, reject) => {
        await this.setState({ isLoading: bool });
        resolve();
      }
    )
  }
  setDate(date) {
    this.setState({ selectedDate: date });
  }
  async componentDidMount() {

    if (this.props.webId) {
      this.setState({ isLoading: true });

      CUPurl = this.props.webId;
      EventUrl = this.props.webId.replace('profile/card#me', '') + 'private/event#';
      await this.getData();
      // convert the users address to lat and long and assign the values to this.state.lat and this.state.lng
      await this.addressToCords();
      // get the daily forcast for specified location
      await this.getDailyForcast();
      // get hourly forecast for current day at specified location
      await this.getHourlyForcast();
      // get events for the day
      await this.getEventData(new Date())
      let fetcher = new $rdf.Fetcher(store);
      // create cup file if not already exists
      try {
        await fetcher.load(CUPurl);
      }
      catch (error) {
        auth.fetch(CUPurl, {
          method: 'PUT', // or 'PUT'
          body: '' // data can be `string` or {object}!
        }).then(res => { return res; })
          .catch();
      }
      this.getProfileData(this.props.webId);
      this.setState({ isLoading: false });
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    // this.setState({isLoading:true});
    if (this.props.webId && this.props.webId !== prevProps.webId) {
      CUPurl = this.props.webId;
      EventUrl = this.props.webId.replace('profile/card#me', '') + 'private/event#';
      await this.getData();
      // convert the users address to lat and long and assign the values to this.state.lat and this.state.lng
      await this.addressToCords();
      // get the daily forcast for specified location
      await this.getDailyForcast();
      // get hourly forecast for current day at specified location
      await this.getHourlyForcast();
      // get event data for today
      await this.getEventData(new Date());
      this.getProfileData(this.props.webId);
    }
    // this.setState({isLoading:false});

  }

  /*
   * This function retrieves a user's card data and tries to grab both the user's name and profile photo if they exist.
   *
   * This is an example of how to use the LDFlex library to fetch different linked data fields.
   */
  getProfileData = async (webid) => {

    let hasImage;


    /*
     * This is an example of how to use LDFlex. Here, we're loading the webID link into a user variable. This user object
     * will contain all of the data stored in the webID link, such as profile information. Then, we're grabbing the user.name property
     * from the returned user object.
     */
    const user = data[this.props.webId];
    const nameLd = await user.name;
    const name = nameLd ? nameLd.value : '';

    let imageLd = await user.image;
    imageLd = imageLd ? imageLd : await user.vcard_hasPhoto;

    let image;
    if (imageLd && imageLd.value) {
      image = imageLd.value;
      hasImage = true;
    } else {
      hasImage = false;
      image = defaultProfilePhoto;
    }
    /*
     * This is where we set the state with the name and image values. The user[hasPhotoContext] line of code is an example of
     * what to do when LDFlex doesn't have the full context. LDFlex has many data contexts already in place, but in case
     * it's missing, you can manually add it like we're doing here.
     *
     * The hasPhotoContext variable stores a link to the definition of the vcard ontology and, specifically, the #hasPhoto
     * property that we're using to store and link the profile image.
     *
     * For more information please go to: https://github.com/solid/query-ldflex
     */
    this.setState({ name, image, isLoading: false, hasImage });
  };

  /**
   * updatedPhoto will update the photo url on vcard file
   * this function will check if user has image or hasPhoto node if not
   * will just update it, the idea is use image instead of hasPhoto
   * @params{String} uri photo url
   */
  updatePhoto = async (uri: String, message) => {
    try {
      const { user } = data;
      this.state.hasImage
        ? await user.image.set(uri)
        : await user.image.add(uri);
      this.props.toastManager.add(['', message], {
        appearance: 'success'
      });
    } catch (error) {
      this.props.toastManager.add(['Error', error.message], {
        appearance: 'error'
      });
    }
  };

  render() {
    var { name, image, isLoading, selectedDate, data } = this.state;
    // if(this.state.weeklyForecast.length === 0 || this.state.hourlyForecast.length === 0) {
    //   return <div />
    // }
    return (
      <WelcomePageContent
        name={name}
        image={image}
        isLoading={isLoading}
        webId={this.props.webId}
        updatePhoto={this.updatePhoto}
        selectedDate={selectedDate}
        loadSetter={this.isLoadingSetter}
        dateSetter={this.setDate}
        data={data}
        weeklyForecast={this.state.weeklyForecast}
        hourlyForecast={this.state.hourlyForecast}
        city={this.state.city}
        state={this.state.state}
        updateLocation={this.updateLocation}
        eventSetter={this.eventSetter}
        events={this.state.events}
        showEvents={this.state.showEvents}
      />
    );
  }
}

export default withWebId(withToastManager(WelcomeComponent));
