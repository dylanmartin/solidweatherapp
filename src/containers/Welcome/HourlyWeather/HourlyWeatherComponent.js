import React from 'react';
import './hourlyWeather.css';

// holds all the svg files for the different weather conditions
// const weatherIcons = {
//     cloudy: '/img/svg/001-cloudy.svg',
//     sunny: '/img/svg/022-sun.svg',
//     rain: '/img/svg/002-rain.svg',
//     storm: '/img/svg/004-storm.svg',
//     partlyCloudy: '/img/svg/044-cloud.svg'
// }
const weatherPictures = {
    cloudy: '/img/weather/clouds-cloudscape-cloudy-158163.jpg',
    sunny: '/img/weather/4k-wallpaper-blue-sky-blur-281260.jpg',
    rain: '/img/weather/car-drops-of-water-glass-1553.jpg',
    storm: '/img/weather/lightning-sky-storm-53459.jpg',
    partlyCloudy: '/img/weather/atmosphere-blue-sky-clouds-912110.jpg',
    fog: '/img/weather/eberhard-grossgasteiger-FlNktHJ4fvM-unsplash.jpg',
}
export default class HourlyWeather extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            units: 'mph',
            show: false,
            date: new Date(),
        }
        this.renderHourlyWeather = this.renderHourlyWeather.bind(this);
        this.getDayString = this.getDayString.bind(this);
        this.getPhoto = this.getPhoto.bind(this);
    }

    getPhoto = () => {
        let condition = this.props.data[0].condition.toString();
        if (condition.toLowerCase().includes('storm')) {
            return (weatherPictures.storm);
        } else if (condition.toLowerCase().includes('fog')) {
            return (weatherPictures.fog);
        }
        else if (condition.toLowerCase().includes('rain') || condition.toLowerCase().includes('shower')) {
            return (weatherPictures.rain);
        }
        else if (condition.toLowerCase().includes('partly cloudy')) {
            return (weatherPictures.partlyCloudy);
        }
        else if (condition.toLowerCase().includes('cloudy')) {
            return (weatherPictures.cloudy);
        } else {
            return (weatherPictures.sunny)
        }
    }

    getDayString = int => {
        let dayNum = int % 7;
        switch (dayNum) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            default:
        }
    }

    getMonthString = int => {
        switch (int) {
            case 0:
                return "January";
            case 1:
                return "February";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5: 
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            case 11:
                return "December";
        }
    }

    renderHourlyWeather = () => {
        let elements = [];
        for(let i = 0; i < this.props.data.length; i++) {
            let hour = this.props.data[i].hour % 12;
            if (hour === 0){
                hour = 12;
            }
            var hourstring = '';
            if(this.props.data[i].hour > 11){
                hourstring =`${hour} PM`;
            }else{
                hourstring =`${hour} AM`;
            }

            elements.push(
                <div id={`BaseContainer${i}`} className='hourlyBaseContainer'>
                    <div className='hourlyDayContainer'>
                        <h4 className='hourDataDisplay'>
                            {hourstring}
                        </h4>
                    </div>
                    <div className='tempContainer'>
                        <h4 className='hourlyDataDisplay'>
                            {this.props.data[i].temp}
                        </h4>
                    </div>
                    <div className='hourlyConditionContainer'>
                        <h4 className='hourlyDataDisplay'>
                            {this.props.data[i].condition}
                        </h4>
                    </div>
                </div>
            );
        }
        return elements;
    }

    render() {
        return (
            <div className='hourlyWeatherContainer' style={{ backgroundImage: `url(${this.getPhoto()})` }} onClick={this.getAditionalInfo}>
                <h4 className='hourlyWeatherHeader'>
                    {this.getDayString(this.state.date.getDay())} {this.getMonthString(this.state.date.getMonth())} {this.state.date.getDate()}
                </h4>
                {this.renderHourlyWeather()}
            </div>
        );
    }
}