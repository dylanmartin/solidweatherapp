import React from 'react';
import './DisplayNav.css';
export default class DisplayNav extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            dispEvents: false,
        }
        this.handleEventClick = this.handleEventClick.bind(this);
        this.handleForecastClick = this.handleForecastClick.bind(this);
    }

    handleForecastClick = () => {
        this.setState({dispEvents:false})
        this.props.eventSetter(false);
        let active = document.getElementById('eventButton');
        let other = document.getElementById('forecastButton');
        active.className = 'eventNavDispButton';
        other.className = 'eventNavDispButtonActive';
    }
    handleEventClick = () => {
        this.setState({dispEvents:true})
        this.props.eventSetter(true);
        let active = document.getElementById('forecastButton');
        let other = document.getElementById('eventButton');
        active.className = 'eventNavDispButton';
        other.className = 'eventNavDispButtonActive';
    }

    render() {
        return(
            <div className='eventNavContainer'>
                <div id="forecastButton" className="eventNavDispButtonActive" onClick={() => this.handleForecastClick()}>
                    Forecast
                </div>
                <div id="eventButton" className="eventNavDispButton" onClick={() => this.handleEventClick()}>
                    Events
                </div>
            </div>
        );
    }

}