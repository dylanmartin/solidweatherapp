import React from 'react';
import './weeklyWeather.css';

// holds all the svg files for the different weather conditions
// const weatherIcons = {
//     cloudy: '/img/svg/001-cloudy.svg',
//     sunny: '/img/svg/022-sun.svg',
//     rain: '/img/svg/002-rain.svg',
//     storm: '/img/svg/004-storm.svg',
//     partlyCloudy: '/img/svg/044-cloud.svg'
// }
const weatherPictures = {
    cloudy: '/img/weather/clouds-cloudscape-cloudy-158163.jpg',
    sunny: '/img/weather/4k-wallpaper-blue-sky-blur-281260.jpg',
    rain: '/img/weather/car-drops-of-water-glass-1553.jpg',
    storm: '/img/weather/lightning-sky-storm-53459.jpg',
    partlyCloudy: '/img/weather/atmosphere-blue-sky-clouds-912110.jpg',
    fog: '/img/weather/eberhard-grossgasteiger-FlNktHJ4fvM-unsplash.jpg',
}
export default class WeeklyWeather extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            units: 'mph',
            show: false,
        }
        this.getAditionalInfo = this.getAditionalInfo.bind(this);
        this.getDayString = this.getDayString.bind(this);
        this.getPhoto = this.getPhoto.bind(this);
    }

    // gets the pictures for the weekly weather
    // this is dumbi boi dylan's idea and it should be improved
    // kinda janky fix later consult smarter people
    getPhoto = () => {
        let condition = this.props.condition.toString();
        if (condition.toLowerCase().includes('fog')) {
            return (weatherPictures.fog);
        }
        else if (condition.toLowerCase().includes('storm')) {
            return (weatherPictures.storm);
        }
        else if (condition.toLowerCase().includes('rain') || condition.toLowerCase().includes('shower')) {
            return (weatherPictures.rain);
        }
        else if (condition.toLowerCase().includes('partly cloudy')) {
            return (weatherPictures.partlyCloudy);
        }
        else if (condition.toLowerCase().includes('cloudy')) {
            return (weatherPictures.cloudy);
        } else {
            return (weatherPictures.sunny)
        }
    }

    getDayString = int => {
        let dayNum = int % 7;
        switch (dayNum) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            default:
        }
    }

    getAditionalInfo = () => {
        let container = document.getElementById(`otherInfoContainer${this.props.index}`);
        if (this.state.show) {
            container.innerHTML = '';
            this.setState({ show: false });

        }
        else {
            for (let key in this.props.otherInfo) {
                if (key === 'windSpeed') {
                    let tmp = document.createElement('div');
                    tmp.innerHTML = `Wind Speed: ${this.props.otherInfo[key]}`;
                    tmp.className = 'otherInfo';
                    container.appendChild(tmp);
                }
                else if (key === 'descriptiveDescription') {
                    let tmp = document.createElement('div');
                    tmp.innerHTML = `Details: ${this.props.otherInfo[key]}`;
                    tmp.className = 'otherInfo';
                    container.appendChild(tmp);
                }
            }
            this.setState({ show: true });
        }
    }


    render() {
        return (
            <div className='weatherContainer' style={{ backgroundImage: `url(${this.getPhoto()})` }} onClick={this.getAditionalInfo}>
                <div className='baseContainer'>
                    <div className='dayContainer'>
                        <h3 className='dataDisplay'>
                            {this.getDayString(this.props.day)}
                        </h3>
                    </div>
                    <div className='tempContainer'>
                        <h3 className='dataDisplay'>
                            {this.props.temperature}
                        </h3>
                    </div>
                    <div className='conditionContainer'>
                        <p className='conditionDisplay'>
                            {this.props.condition}
                        </p>
                    </div>
                </div>
                {/* This div is where we append any other  */}
                <div className='otherInfoContainer' id={`otherInfoContainer${this.props.index}`} />
            </div>
        );
    }
}