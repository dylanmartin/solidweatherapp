import React from "react";
import { NavLink } from "react-router-dom";
import './navigation.css';
type Props = {
  navigation: Object
};

const Navigation = ({ navigation }: Props) => {
  return (
    <nav role="navigation">
      <ul>
        {navigation &&
          navigation.map(item => (
              <NavLink to={item.to} className='navlink' activeClassName="navactive">{item.label}</NavLink>
          ))}
      </ul>
    </nav>
  );
};

export default Navigation;
