# Solid Calendar App

### Summary:

**Note**: This app uses the national weather service API so it is currently only able to fetch weather in the United States.

Gives the user a 12 hour weather forecast and a wekly weather forecast. On load the app uses the users home location stored in their Common User Profile. Users can also search the weather in a city or state by typing the city name followed by the state name or two letter state code. Solid weather is also linked to the solid calendar so it will also display the events a user has scheduled for that day in the city. If you hover over the event it will also highlight the weather for the event in color. 

**Note:** This is an early alpha version of the app

### Installiation and Setup:

Clone the repository, cd into directory, install dependencies, and run

```bash
 git clone https://dylanmartin@bitbucket.org/dylanmartin/solidweatherapp.git
 cd solid-calendar
 npm install
 npm run start
```
